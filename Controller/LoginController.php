<?php
    /*
        Fuente: LoginController.php
        ICONT WEB - BACKEND
        CURSO UDEMY - PHP y MySQL - Crea un sistema de Ventas POS + Facturación
        Profesor : Henry Josph Calani Ambrocio
        Desarrollador WEB Gustavo E. Romay
        Inicio : 26/02/2021
        Finalización : 26/02/2021
        Ultima Actualización: 26/02/2021
        información: Controlador - Llama a la Vista para el Ingreso al Sistema.
    */

    require('Views/LoginView.php')
?>